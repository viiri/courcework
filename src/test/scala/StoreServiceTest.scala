import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.dvlife.x0r.entity.{AppEntity, NotificationEntity}
import ru.dvlife.x0r.repository.InMemoryNotificationRepository
import ru.dvlife.x0r.repository.store.StoreRepository
import ru.dvlife.x0r.service.StoreServiceImpl

import scala.concurrent.Future

class StoreServiceTest extends AsyncFlatSpec with Matchers with AsyncMockFactory {
  private val mockStoreRepository = mock[StoreRepository]

  it should "return search results" in {
    val storeService = new StoreServiceImpl(mockStoreRepository, new InMemoryNotificationRepository)

    (mockStoreRepository.findAllByNameContaining _)
      .expects("game")
      .returns(Future.successful(Seq("Game Name", "Game2")))

    storeService.searchApps("game").map { result =>
      result shouldBe Seq("Game Name", "Game2")
    }
  }

  it should "create notifications" in {
    val storeService = new StoreServiceImpl(mockStoreRepository, new InMemoryNotificationRepository)

    val chatId = 666
    val appEntity = AppEntity("Steam", "Game2", Some(66600))

    (mockStoreRepository.findByName _)
      .expects("Game2")
      .returns(Future.successful(Option(appEntity)))

    (for {
      _ <- storeService.createNotification(chatId, appEntity.appName)
      r <- storeService.listNotifications(chatId)
      app = r.map(_.appEntity)
    } yield app).map(_ shouldBe Seq(appEntity))
  }

  it should "send notifications" in {
    val storeService = new StoreServiceImpl(mockStoreRepository, new InMemoryNotificationRepository)

    val chatId = 666
    val gameName = "Game2"
    val price = 66600
    val lessPrice = price / 2

    val appEntity = AppEntity("Steam", gameName, Some(price))

    (mockStoreRepository.findByName _)
      .expects(gameName)
      .returns(Future.successful(Option(appEntity)))

    (mockStoreRepository.findByAppEntitiesAndPriceLess _)
      .expects(Seq(appEntity))
      .returns(Future.successful(Seq(appEntity.copy(price = Some(lessPrice)))))

    val notifications = scala.collection.mutable.ListBuffer[NotificationEntity]()
    def addNotification(notificationEntity: NotificationEntity): Unit = {
      notifications += notificationEntity
    }

    (for {
      _ <- storeService.createNotification(chatId, appEntity.appName)
      _ <- Future.successful(storeService.sendNotifications(addNotification))
      apps = notifications.map(_.appEntity).toList
    } yield apps).map { _ =>
      notifications.flatMap(_.appEntity.price) shouldBe Seq(lessPrice)
    }
  }

  it should "clear notifications" in {
    val storeService = new StoreServiceImpl(mockStoreRepository, new InMemoryNotificationRepository)

    val chatId = 666
    val appEntity = AppEntity("Steam", "Game2", Some(66600))

    (mockStoreRepository.findByName _)
      .expects("Game2")
      .returns(Future.successful(Option(appEntity)))

    (for {
      _ <- storeService.createNotification(chatId, appEntity.appName)
      c <- storeService.clearNotifications(chatId)
    } yield c).map(_ shouldBe 1)
  }
}

