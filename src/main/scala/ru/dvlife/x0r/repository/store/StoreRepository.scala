package ru.dvlife.x0r.repository.store

import ru.dvlife.x0r.entity.AppEntity

import scala.concurrent.Future

trait StoreRepository {
  def findAllByNameContaining(appName: String): Future[Seq[String]]
  def findByName(appName: String): Future[Option[AppEntity]]
  def findByAppEntitiesAndPriceLess(appEntities: Seq[AppEntity]): Future[Seq[AppEntity]]
  def refreshCache(): Future[Unit]
}
