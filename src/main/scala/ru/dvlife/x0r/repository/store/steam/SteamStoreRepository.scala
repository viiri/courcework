package ru.dvlife.x0r.repository.store.steam

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import ru.dvlife.x0r.entity.AppEntity
import ru.dvlife.x0r.repository.store.StoreRepository

import scala.collection.concurrent.TrieMap
import scala.concurrent.{ExecutionContext, Future}

class SteamStoreRepository(implicit val system: ActorSystem, val materializer: ActorMaterializer, val executionContext: ExecutionContext) extends StoreRepository {
  private val STORE_ID = "Steam"
  private val cache = TrieMap[String, (SteamApp, Option[SteamAppPrice])]()

  private def makeAppEntity(steamApp: SteamApp, steamAppPrice: Option[SteamAppPrice] = None) = {
    AppEntity(STORE_ID, steamApp.name, steamAppPrice.map(_.finalPrice))
  }

  override def findAllByNameContaining(appName: String): Future[Seq[String]] =
    Future.successful(cache
      .values
      .toSeq
      .map(_._1.name)
      .filter(_.toLowerCase.startsWith(appName.toLowerCase))
      .take(15)
    )

  override def findByName(appName: String): Future[Option[AppEntity]] = {
    def loadPrice(steamApp: SteamApp): Future[Option[SteamAppPrice]] = {
      val uri = s"https://store.steampowered.com/api/appdetails/?appids=${steamApp.appid}&filters=price_overview"

      for {
        response <- fetchJson(uri)
        steamAppPrices <- Unmarshal(response).to[Map[Long, SteamAppPrice]]
      } yield steamAppPrices.get(steamApp.appid)
    }

    cache.get(appName) match {
      case Some((steamApp, steamAppPrice@Some(_))) =>
        Future.successful(Some(makeAppEntity(steamApp, steamAppPrice)))
      case Some((steamApp, None)) =>
        loadPrice(steamApp).map {
          case steamAppPrice@Some(_) =>
            cache.replace(appName, (steamApp, steamAppPrice))
            Some(makeAppEntity(steamApp, steamAppPrice))
          case _ =>
            None
        }
      case _ =>
        Future.successful(None)
    }
  }

  def findByAppEntitiesAndPriceLess(appEntities: Seq[AppEntity]): Future[Seq[AppEntity]] = {
    val appsLut = appEntities.collect {
      case AppEntity(STORE_ID, appName, Some(price)) if cache.contains(appName) =>
        appName -> price
    }.toMap

    appsLut.keys.flatMap(cache.get).foreach {
      case (steamApp, _) => cache.replace(steamApp.name, (steamApp, None))
    }

    for {
      sa <- Future.sequence(appsLut.map(item => findByName(item._1)))
      ae = sa.flatten.collect {
        case res@AppEntity(_, appName, Some(price))
          if appsLut.get(appName).exists(_ > price) =>
            res
      }.toSeq
    } yield ae
  }

  override def refreshCache(): Future[Unit] = {
    val uri = "http://api.steampowered.com/ISteamApps/GetAppList/v0001"

    for {
      response <- fetchJson(uri)
      steamApps <- Unmarshal(response).to[List[SteamApp]]
    } yield {
      cache.clear()
      steamApps.foreach {
        item =>
          cache.putIfAbsent(item.name, (item, None))
      }
    }
  }

  private def fetchJson(uri: String): Future[HttpResponse] = {
    val responseFuture: Future[HttpResponse] = Http().singleRequest(Get(uri))

    for {
      response <- responseFuture
      if response.status.isSuccess()
    } yield response
  }
}
