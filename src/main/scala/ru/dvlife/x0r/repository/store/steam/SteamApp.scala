package ru.dvlife.x0r.repository.store.steam

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class SteamApp(appid: Long, name: String)

object SteamApp {
  implicit val steamAppDecoder: Decoder[SteamApp] = deriveDecoder
  implicit val steamAppListDecoder: Decoder[List[SteamApp]] = Decoder.decodeList[SteamApp].prepare(
    _.downField("applist").downField("apps").downField("app")
  )
}
