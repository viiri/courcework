package ru.dvlife.x0r.repository.store.steam

import io.circe.Decoder

//https://store.steampowered.com/api/appdetails/?appids=10,20,30,40&filters=price_overview
case class SteamAppPrice(currency: String,
                         initialPrice: Long,
                         finalPrice: Long,
                         discountPercent: Long,
                         initialFormatted: String,
                         finalFormatted: String
                        )

object SteamAppPrice {
  implicit val steamAppDecoder: Decoder[SteamAppPrice] =
    Decoder.forProduct6(
      "currency",
      "initial",
      "final",
      "discount_percent",
      "initial_formatted",
      "final_formatted")(SteamAppPrice.apply)
      .prepare(
        _.downField("data").downField("price_overview")
      )
}

