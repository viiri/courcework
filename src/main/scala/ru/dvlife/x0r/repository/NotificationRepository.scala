package ru.dvlife.x0r.repository

import java.time.Instant

import ru.dvlife.x0r.entity.{AppEntity, NotificationEntity}

import scala.concurrent.Future

trait NotificationRepository {
  def init(): Future[Unit] = Future.unit
  def findAll(): Future[Seq[NotificationEntity]]
  def findByAppName(appName: String): Future[Seq[NotificationEntity]]
  def findByChatId(chatId: Long): Future[Seq[NotificationEntity]]
  def insert(createdAt: Instant, chatId: Long, appEntity: AppEntity): Future[NotificationEntity]
  def delete(notificationEntity: NotificationEntity): Future[Int]
  def deleteByChatId(chatId: Long): Future[Int]
}
