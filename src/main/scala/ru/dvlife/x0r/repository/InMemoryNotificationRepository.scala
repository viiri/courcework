package ru.dvlife.x0r.repository

import java.time.Instant
import java.util.concurrent.ThreadLocalRandom

import ru.dvlife.x0r.entity.{AppEntity, NotificationEntity}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

class InMemoryNotificationRepository extends NotificationRepository {
  private val storage = TrieMap[Long, NotificationEntity]()

  override def findAll(): Future[Seq[NotificationEntity]] = {
    Future.successful(storage.values.toSeq)
  }

  override def insert(createdAt: Instant, chatId: Long, appEntity: AppEntity): Future[NotificationEntity] = {
    val id = ThreadLocalRandom.current().nextLong(Long.MaxValue)
    val notificationEntity = NotificationEntity(id, createdAt, chatId, appEntity)

    storage.putIfAbsent(id, notificationEntity) match {
      case Some(_) =>
        Future.failed(new Exception("Notification already exists!"))
      case _ =>
        Future.successful(notificationEntity)
    }
  }

  override def findByAppName(appName: String): Future[Seq[NotificationEntity]] = {
    Future.successful(storage.values.filter(_.appEntity.appName == appName).toSeq)
  }

  override def findByChatId(chatId: Long): Future[Seq[NotificationEntity]] = {
    Future.successful(storage.values.filter(_.chatId == chatId).toSeq)
  }

  override def delete(notificationEntity: NotificationEntity): Future[Int] = {
    Future.successful(storage.remove(notificationEntity.id).map(_ => 1).getOrElse(0))
  }

  override def deleteByChatId(chatId: Long): Future[Int] = {
    val toDelete = storage.values.filter(_.chatId == chatId).toSeq

    Future.successful {
      toDelete.flatMap(item => storage.remove(item.id)).size
    }
  }
}
