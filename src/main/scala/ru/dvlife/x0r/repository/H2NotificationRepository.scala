package ru.dvlife.x0r.repository

import ru.dvlife.x0r.entity.{AppEntity, NotificationEntity}
import slick.dbio.Effect
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

import java.time.Instant
import scala.concurrent.{ExecutionContext, Future}

class NotificationsTable(tag: Tag) extends Table[NotificationEntity](tag, "NOTIFICATIONS") {
  def notificationId: Rep[Long] = column("ID", O.PrimaryKey, O.AutoInc)
  def createdAt: Rep[Instant] = column("CREATED_AT")
  def chatId: Rep[Long] = column("CHAT_ID")
  def storeId: Rep[String] = column("STORE_ID")
  def appName: Rep[String] = column("APP_NAME")
  def price: Rep[Option[Long]] = column("PRICE")

  override def * : ProvenShape[NotificationEntity] = (notificationId, createdAt, chatId, (storeId, appName, price)).shaped <> (
    {
      case (notificationId, createdAt, chatId, appEntity) =>
        NotificationEntity(notificationId, createdAt, chatId, AppEntity.tupled(appEntity))
    },
    {
      n: NotificationEntity =>
        Some(n.id, n.createdAt, n.chatId, AppEntity.unapply(n.appEntity).get)
    }
  )
}

class H2NotificationRepository(db: Database)(implicit val executionContext: ExecutionContext) extends NotificationRepository {

  override def init(): Future[Unit] = db.run(H2NotificationQueries.createSchema)

  override def findAll(): Future[Seq[NotificationEntity]] =
    db.run(H2NotificationQueries.findAll())

  override def findByAppName(appName: String): Future[Seq[NotificationEntity]] =
    db.run(H2NotificationQueries.findByAppName(appName))

  override def findByChatId(chatId: Long): Future[Seq[NotificationEntity]] =
    db.run(H2NotificationQueries.findByChatId(chatId))

  override def insert(createdAt: Instant, chatId: Long, appEntity: AppEntity): Future[NotificationEntity] = {
    db.run(H2NotificationQueries.insert(NotificationEntity(0L, createdAt, chatId, appEntity)))
  }

  override def delete(notificationEntity: NotificationEntity): Future[Int] =
    db.run(H2NotificationQueries.delete(notificationEntity))

  override def deleteByChatId(chatId: Long): Future[Int] =
    db.run(H2NotificationQueries.deleteByChatId(chatId))
}

object H2NotificationQueries {
  val Notifications = TableQuery[NotificationsTable]

  def createSchema: DBIOAction[Unit, NoStream, Effect.Schema] =
    H2NotificationQueries.Notifications.schema.create

  def findAll(): DBIOAction[Seq[NotificationEntity], NoStream, Effect.Read] =
    Notifications
      .result

  def findByAppName(appName: String): DBIOAction[Seq[NotificationEntity], NoStream, Effect.Read] =
    Notifications
      .filter(_.appName === appName)
      .result

  def findByChatId(chatId: Long): DBIOAction[Seq[NotificationEntity], NoStream, Effect.Read] =
    Notifications
      .filter(_.chatId === chatId)
      .result

  def insert(notificationEntity: NotificationEntity): DBIOAction[NotificationEntity, NoStream, Effect.Write] =
    (Notifications returning Notifications.map(_.notificationId)
      into ((notificationEntity, id) => notificationEntity.copy(id = id)) += notificationEntity)

  def delete(notificationEntity: NotificationEntity): DBIOAction[Int, NoStream, Effect.Write] =
    Notifications
      .filter(_.notificationId === notificationEntity.id)
      .delete

  def deleteByChatId(chatId: Long): DBIOAction[Int, NoStream, Effect.Write] =
    Notifications
      .filter(_.chatId === chatId)
      .delete
}
