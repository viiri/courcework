package ru.dvlife.x0r.service

import java.time.Instant

import ru.dvlife.x0r.entity.NotificationEntity
import ru.dvlife.x0r.repository.NotificationRepository
import ru.dvlife.x0r.repository.store.StoreRepository

import scala.concurrent.{ExecutionContext, Future}

class StoreServiceImpl(storeRepository: StoreRepository, notificationRepository: NotificationRepository)(implicit val executionContext: ExecutionContext) extends StoreService {
  override def searchApps(appName: String): Future[Seq[String]] = {
    storeRepository
      .findAllByNameContaining(appName)
  }

  override def createNotification(chatId: Long, appName: String): Future[NotificationEntity] = {
    storeRepository
      .findByName(appName)
      .flatMap {
        case Some(value) =>
          notificationRepository.insert(Instant.now, chatId, value)
        case _ =>
          Future.failed(new Exception("Application not found!"))
      }
  }

  override def sendNotifications(f: NotificationEntity => Unit): Unit = {
    for {
      notifications <- notificationRepository.findAll()
      apps <- storeRepository.findByAppEntitiesAndPriceLess(notifications.map(_.appEntity))
      appsLut = apps.map(item => item.appName -> item).toMap
    } {
      notifications.foreach {
        case ne@NotificationEntity(_, _, _, appEntity) =>
            if(appsLut.contains(appEntity.appName)) {
              f(ne.copy(appEntity = appsLut(appEntity.appName)))
              notificationRepository.delete(ne)
            }
      }
    }
  }

  override def listNotifications(chatId: Long): Future[Seq[NotificationEntity]] = {
    notificationRepository.findByChatId(chatId)
  }

  override def clearNotifications(chatId: Long): Future[Int] = {
    notificationRepository.deleteByChatId(chatId)
  }
}
