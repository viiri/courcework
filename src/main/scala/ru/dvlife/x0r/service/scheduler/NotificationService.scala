package ru.dvlife.x0r.service.scheduler

import akka.actor.{ActorSystem, Cancellable}
import com.bot4s.telegram.methods.SendMessage
import ru.dvlife.x0r.bot.StoreBot
import ru.dvlife.x0r.entity.NotificationEntity
import ru.dvlife.x0r.service.StoreService

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{Duration, FiniteDuration}

class NotificationService(bot: StoreBot, storeService: StoreService)(implicit val system: ActorSystem, val executionContext: ExecutionContext) extends SchedulerService {
  private lazy val sendMessage: NotificationEntity => Unit =
    ne =>
      bot.request(SendMessage(ne.chatId, s"${ne.appEntity.appName}: ${ne.appEntity.priceToString}")).map(_ => ())

  override def schedule(interval: FiniteDuration): Cancellable = system.scheduler.schedule(Duration.Zero, interval,
    new Runnable {
      override def run(): Unit = {
        storeService.sendNotifications(sendMessage)
      }
    })
}
