package ru.dvlife.x0r.service.scheduler

import akka.actor.Cancellable

import scala.concurrent.duration.FiniteDuration

trait SchedulerService {
  def schedule(interval: FiniteDuration): Cancellable
}
