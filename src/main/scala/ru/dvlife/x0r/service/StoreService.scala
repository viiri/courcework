package ru.dvlife.x0r.service

import ru.dvlife.x0r.entity.NotificationEntity

import scala.concurrent.Future

trait StoreService {
  def searchApps(appName: String): Future[Seq[String]]
  def createNotification(chatId: Long, appName: String): Future[NotificationEntity]
  def sendNotifications(f: NotificationEntity => Unit): Unit
  def listNotifications(chatId: Long): Future[Seq[NotificationEntity]]
  def clearNotifications(chatId: Long): Future[Int]
}
