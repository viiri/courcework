package ru.dvlife.x0r.entity

case class AppEntity(storeId: String, appName: String, price: Option[Long] = None) {
  def priceToString: String = {
    price match {
      case Some(v) =>
        val hi = v / 100
        val lo = v % 100

        s"$hi.$lo"
      case None =>
        "0"
    }
  }
}
