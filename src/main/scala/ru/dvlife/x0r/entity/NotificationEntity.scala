package ru.dvlife.x0r.entity

import java.time.Instant

case class NotificationEntity(id: Long, createdAt: Instant, chatId: Long, appEntity: AppEntity)
