package ru.dvlife.x0r.bot.commands

import com.bot4s.telegram.api.declarative.Commands
import ru.dvlife.x0r.bot.StoreBot
import ru.dvlife.x0r.service.StoreService

import scala.concurrent.Future

trait StoreBotCommands extends Commands[Future] {
  this: StoreBot =>

  val storeService: StoreService

  onCommand('search) { implicit msg =>
    withArgs { args =>
      for {
        _ <- typing
        apps <- storeService.searchApps(args.mkString(" "))
        _ <- reply(apps.mkString("\n"))
      } yield ()
    }
  }

  onCommand('notify) { implicit msg =>
    withArgs { args =>
      for {
        _ <- typing
        n <- storeService.createNotification(msg.source, args.mkString(" "))
        _ <- reply(s"Notification for game '${n.appEntity.appName}' is created. Starting price is ${n.appEntity.priceToString}.")
      } yield ()
    }
  }

  onCommand('clear) { implicit msg =>
    withArgs { args =>
      for {
        _ <- typing
        n <- storeService.clearNotifications(msg.source)
        _ <- reply(s"$n notification(s) deleted.")
      } yield ()
    }
  }

  onCommand('show) { implicit msg =>
    for {
      _ <- typing
      n <- storeService.listNotifications(msg.source)
      _ <- reply(n.map(_.appEntity.appName).mkString("\n"))
    } yield ()
  }

  onCommand('start | 'help) { implicit msg =>
    for {
      _ <- typing
      _ <- reply(
        """
          |/search - search by partial name
          |/notify - create notification by full name
          |/show - list of active notifications
          |/clear - clear active notifications
          |""".stripMargin
      )
    } yield ()
  }
}
