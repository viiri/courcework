package ru.dvlife.x0r.bot

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import cats.MonadError
import cats.instances.future._
import com.bot4s.telegram.api._
import com.bot4s.telegram.api.declarative.Commands
import com.bot4s.telegram.clients.AkkaHttpClient
import com.bot4s.telegram.future.{BotExecutionContext, Polling}
import ru.dvlife.x0r.bot.commands.StoreBotCommands
import ru.dvlife.x0r.service.{StoreService, StoreServiceImpl}
import slogging.{LogLevel, LoggerConfig, PrintLoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

class StoreBot(token: String)(val storeService: StoreServiceImpl)(implicit val system: ActorSystem, val materializer: ActorMaterializer, val executionContext: ExecutionContext)
  extends BotBase[Future]
    with BotExecutionContext
    with AkkaImplicits
    with Polling
    with StoreBotCommands
    with ChatActions[Future] {

  LoggerConfig.factory = PrintLoggerFactory()
  LoggerConfig.level = LogLevel.TRACE

  override val monad = MonadError[Future, Throwable]
  override val client = new AkkaHttpClient(token)
}

