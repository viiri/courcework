package ru.dvlife.x0r

import ru.dvlife.x0r.Implicits._
import ru.dvlife.x0r.bot.StoreBot
import ru.dvlife.x0r.repository.H2NotificationRepository
import ru.dvlife.x0r.repository.store.steam.SteamStoreRepository
import ru.dvlife.x0r.service.StoreServiceImpl
import ru.dvlife.x0r.service.scheduler.NotificationService
import slick.jdbc.JdbcBackend.Database

import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.io.StdIn

object Main {
  def main(args: Array[String]): Unit = {
    val db = Database.forConfig("notifications")
    val notificationRepository = new H2NotificationRepository(db)
    val storeRepository = new SteamStoreRepository
    val storeService = new StoreServiceImpl(storeRepository, notificationRepository)
    val bot = new StoreBot("603342516:AAGXEMMsA8JaQLoUqxhmPLgC7wTE4Qarr34")(storeService)
    val notificationService = new NotificationService(bot, storeService).schedule(Duration(1, TimeUnit.HOURS))

    val eol = for {
      _ <- notificationRepository.init()
      _ <- storeRepository.refreshCache()
      b <- bot.run()
    } yield b

    println("Press [ENTER] to shutdown the bot, it may take a few seconds...")
    StdIn.readLine()

    notificationService.cancel()
    bot.shutdown()
    db.close()

    Await.result(eol, Duration.Inf)
    println("Bot stopped.")
  }
}
