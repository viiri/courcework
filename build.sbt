name := "courcework"

version := "0.1"

scalaVersion := "2.12.12"

libraryDependencies ++= Seq(
  "com.bot4s" %% "telegram-core" % "4.4.0-RC2" withSources() withJavadoc(),
  "com.bot4s" %% "telegram-akka" % "4.4.0-RC2" withSources() withJavadoc(),
  "de.heikoseeberger" %% "akka-http-circe" % "1.27.0",
  "com.typesafe.slick" %% "slick" % "3.3.3",
  "org.slf4j" % "slf4j-simple" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
  "com.h2database" % "h2" % "1.4.200",
  "org.scalatest" %% "scalatest" % "3.2.0" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test
)

